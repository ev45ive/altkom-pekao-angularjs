angular.module('myapp')
.directive('tabs',function() {

	return {
		//restrict:'EA',
		// template: function($elem, attrs){
		// 	console.log($elem,attrs);
		// 	return 'Test'
		// }
		template:'<ul class="list-group">\
			<li class="list-group-item"\
			 ng-repeat="tab in $ctrl.tabs"\
			ng-click="$ctrl.select(tab)">\
				{{tab.title}}\
			</li>\
		</ul>\
		<ng-transclude></ng-transclude>',
		//priority:0,
		transclude:true,
		bindings:{
			onChange:'&'
		},
		controller: function TabsCtrl(){
			this.tabs = []
			this.addTab = function(tab){
				if(!this.selected){
					this.select(tab)
				}
				this.tabs.push(tab)
				console.log('tabs',this)
			}
			this.select = function(tab){
				this.tabs.forEach(tab=>{
					tab.selected = false
				})
				this.selected = tab.selected = true;
				this.onChange({tab: tab})
			}
		},
		controllerAs:'$ctrl'
	}
})
.directive('tab',function() {
	return {
		template:`<div class="panel-default" 
		ng-show="$ctrl.selected">
		{{$ctrl.title}}
			<div class="panel-body" ng-transclude>
			</div>
		</div>`,
		transclude: true,
		require:{Tabs:'^tabs'},
		scope:true,
		bindings:{	
			title:'<',
			// @ - as string
			// = - 2-way binding
			// < - 1-way binding
			// & - callback binding
		},
		bindToController:true,
		controller: function($attrs){
			this.title = $attrs.title
			this.$onInit = function(){
				console.log('Tab',this)
				this.Tabs.addTab(this);
			}
		},
		//$scope.$ctrl = controller.this
		controllerAs:'$ctrl',
		// link:function(scope,$element,attrs, ctrls){
		// 	console.log(arguments)
		// 	ctrls.
		// },
	}
})