
angular.module('tasks')
.service('Todos', Todos)

function Todos(){
	this.data = {
		tasks: [
			{name:'Zakupy', completed: true},
			{name:'Nauczyć się Angulara!', completed: false},
		],
		completed: []
	}

	this.addTask = function(task){
		this.data.tasks.push( task )
	}

	this.archiveCompleted = function(){
		var groups = this.data.tasks.reduce(function(group,task){
			group[task.completed? 'completed':'tasks'].push(task);
			return group
		},{ completed:[], tasks:[]});
		this.data.tasks = groups.tasks
		this.data.completed = groups.completed;
	}
}