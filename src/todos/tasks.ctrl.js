
angular.module('tasks')
.controller('TasksCtrl', function($scope, Todos, $injector) {
	"ngInject";

	//console.log('Tasks', $scope)

	function createNew(){
		$scope.task = {
			name: '',
			completed: false
		} 
	}
	createNew()

	$scope.data = Todos.data;

	$scope.archiveCompleted = function(){
		Todos.archiveCompleted()
	}

	$scope.addTask = function(){
		Todos.addTask($scope.task)
		createNew()
	}
})