
angular.module('myapp',['ngMessages','tasks','users','ui.router'])

.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/users')

	$stateProvider
	.state('root',{
		url:'/',
		templateUrl:'layout.tpl.html'
	})
	.state('root.users',{
		url:'users',
		templateUrl:'users/users.tpl.html'
	})	
		.state('root.users.show',{
			url:'/:id',
			// resolve:{
			// 	users: function(Users) {
			// 		return Users.getUsers()
			// 	},
			// 	user: function(Users, $stateParams) {
			// 		console.log($stateParams, users, Users.data.users)
			// 		return users.find( user => user.id == $stateParams.id )
			// 	}
			// },
			template:'<user-detail data="$resolve.users[0]"></user-detail>'
		})
	.state('root.tasks',{
		url:'tasks',
		templateUrl:'todos/tasks.tpl.html'
	})
	.state('root.tabs',{
		url:'tabs',
		templateUrl:'tabs/tabs.tpl.html'
	})

})

.constant('config',{
	url: 'http://localhost:3000/users'
})

.provider('Session',function(){
	var opptions = {};

	return {
		setConfigOption: function(){},
		$get: function(session){
			
			return {
				options: opptions,
				session:session	
			}
		}
	}
})

.config(function($httpProvider, SessionProvider){

	SessionProvider.setConfigOption();


	$httpProvider.interceptors.push(function(Session){
		return {
			// 'responseError':function(rejection){

			// 	//return $http
			// },
			'request': function(config){
				//console.log(config, Session);
				config.headers['Authorization'] = 'Bearer '+Session.session.token;
				return config;
			}
		}
	})	
})

.value('session',{
	token: 'lubieplackimalinowe'
})
.run(function($http){

})



.directive('startsWithAlpha', function($http, $q) {
	return {
		restrict:'A',
		require:{ngModelCtrl:'ngModel',ngForm:'^form'},
		link: function(scope, element, attrs, ctrls){
			ctrls.ngModelCtrl.$validators['alpha'] = function(modelValue,value){
				return /^[a-zA-Z].*$/.test(value)
			}
			console.log(ctrls.ngModelCtrl, ctrls.ngForm)

			// ctrls.ngModelCtrl.$asyncValidators['async'] = function(value){
			// 	return $q(function(resolve){
			// 		setTimeout(function(){
			// 			resolve(true)
			// 		},100)
			// 	})
			// }

		}
	}
})