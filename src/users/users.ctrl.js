angular.module('users')
.controller('UsersCtrl', function($scope, $http, Users){

	// $scope.user = {
	// 	name: 'Test 4',
	// 	email: 'test',
	// 	address:{
	// 		city:'test'
	// 	}
	// }
	$scope.users = []

	$scope.data = []

	$scope.mapClick = function(message){
		console.log('mapClick', message)
	}

	$scope.saveUser = function(user){
		console.log('Saved user', user)
		Users.saveUser(user).then(function(user){
			$scope.user = user;
		})
	}

	$scope.select = function(user){
		$scope.user = user;
	}

	$scope.getUsers = function(){
		Users.getUsers()
		.then(function(users){
			$scope.users = users;
		} )
	}

	$scope.getUsers();

})