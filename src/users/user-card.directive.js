angular.module('users')
	.directive('userCard', function() {
		
		// Directive definition object ;-)
		return {
			restrict:'EA',
			templateUrl: 'users/user.tpl.html',
			scope:{
				user: '=data'
			}
		}
	})