angular.module('users')
.factory('Users',function($http, $q, config) {
	var url = config.url;

	var data = {
		users: []
	}


	// var users = JSON.parse(localStorage.getItem('users'))
	// if(users){
	// 	data.users = users;
	// }

	function saveUser(user){
		return $http.put(url+'/'+user.id,user)
		.then(function(response){
			return response.data
		} )
	}

	function getUsers(){
		// $q(function(resolve){
		// 	resolve(this.users)
		// })

		return (data.users.length? $q.when(data.users) : $http.get(url)
		.then(function(response){
			data.users = response.data
			return response.data;
		})
		.then(function(users){
			localStorage.setItem('users', JSON.stringify(users))
			return users;
		})
		)
	}

	return {
		data: data,
		saveUser: saveUser,
		getUsers: getUsers
	}
})