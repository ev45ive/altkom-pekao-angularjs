angular.module('users')
.component('userForm',{
	templateUrl:'users/user-form.tpl.html',
	bindings:{
		data: '<data',
		showing:'=',
		onSave: '&'
	},
	transclude:true,
	controller: UserFormCtrl
	//controllerAs:'$ctrl',
	//scope:true
})
//.controller('');

function UserFormCtrl() {
	//this.user

	this.$onChanges = function(){
		this.user = Object.assign({},this.data);
	}

	this.save = function(){
		// saveUser
		this.onSave({user:this.user, innedane:true})
	}
}