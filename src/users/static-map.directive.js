angular.module('users')
	.directive('staticMap', function($interval) {
		
		// Directive definition object ;-)
		return {
			restrict:'EA',
			template: '<div>\
				<img>\
			</div>',
			// scope:{
			// 	lat: '=',
			// 	lng: '='
			// },
			link: function(scope, $element, attrs){

				var image = $element.find('img');
				var geo = {
					lat: '52.17969', lng:'20.9940'
				}

				$element.find('input').on('keyup', function(event){
					scope.$apply(function(){
						scope.title = event.target.value;

					})
				})

				var i = 0;
				$interval(function(){
						scope.title = i; i++;
				},500)

				image.on('click', function(event){
					console.log(event)

					var mapClickAttr = attrs.mapClick;
					scope.$eval(mapClickAttr, {
						message: 'Hello!'
					})
				})

				function getUrl(lat,lng){
					return 'https://maps.googleapis.com/maps/api/staticmap'
					+'?center='+lng+','+lat+'&size=300x300&zoom=4'
					+'&markers=color:blue%7Clabel:A%7C52,21'
				}
				function updateImage(url){
					image.attr('src',url)
				}

				scope.$watch(attrs.lat, function(newValue,oldValue){
					geo.lat = newValue
					var url = getUrl(geo.lat,geo.lng)
					updateImage(url)
				})
				scope.$watch(attrs.lng, function(newValue,oldValue){
					geo.lng = newValue
					var url = getUrl(geo.lat,geo.lng)
					updateImage(url)
				})
			}
		}
	})