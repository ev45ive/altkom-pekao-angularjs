var gulp = require('gulp');
var browserSync = require('browser-sync')
var server = browserSync.create()
var wiredep = require('gulp-wiredep')
var debug = require('gulp-debug')
var rename = require('gulp-rename')
var inject = require('gulp-inject')
var watch = require('gulp-watch')
var useref = require('gulp-useref')

gulp.task('default',['copy'], function() {
	console.log('hello world!')
})

gulp.task('serve', ['inject'], function(){
	server.init({
		server:{
			baseDir:'./src/'
		},
		port:8080
	})

	gulp.watch('./src/index.tpl.html').on('change',function(){
		gulp.run(['inject'], function(){
			server.reload()
		})
	})

	watch(['./src/**/*.js'], {events:['add','unlink']}, function(){
		gulp.run(['inject'], server.reload)
	})

	watch(['./src/index.tpl.html'], {events:['change']}, function(){
		gulp.run(['inject'], server.reload)
	})

	watch([
		'./src/**/*.js',
		'./src/**/*.html',
		'!./src/bower_components'
	],{events:['change']},function(){
		server.reload()
	})
})

gulp.task('build', ['inject'], function(){

	return gulp.src('./src/index.html')
		.pipe(useref({

		}))
		.pipe(gulp.dest('dist'))
})

gulp.task('inject', function(){
	return gulp.src('./src/index.tpl.html')
		.pipe(rename('index.html'))
		.pipe(wiredep({
			//"directory":"src/bower_components"
		}))
		.pipe(inject(
			gulp.src([
				'!./src/**/app.js',
				'./src/**/*.module.js', 
				'./src/**/*.js', 
				'!./src/bower_components/**'
			]), { relative: true }
		))
		//.pipe(debug())
		.pipe(gulp.dest('./src/'))
})

gulp.task('copy', function() {
	return gulp
	.src(['./src/**'])
	//.pipe(...)
	.pipe( gulp.dest('./dist') )
})

